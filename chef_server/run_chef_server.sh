#!/bin/sh

export PATH=/opt/opscode/embedded/bin/:$PATH

/opt/opscode/embedded/bin/runsvdir-start &
chef-server-ctl reconfigure

mkdir /root/.chef
chef-server-ctl user-create chefadmin chef admin chefadmin@gmail.com '123456' --filename /root/.chef/chefadmin.pem
chef-server-ctl org-create chef-on-ubuntu "Chef Infrastructure on Ubuntu 14.04" --association_user chefadmin --filename /root/.chef/chef-on-ubuntu.pem

knife ssl fetch

knife cookbook upload --all -o /cookbooks -V

touch /chef_server_started.flag

tail -F /opt/opscode/embedded/service/*/log/current
