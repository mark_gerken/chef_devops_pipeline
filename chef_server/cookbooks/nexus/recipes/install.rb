cookbook_file '/nexus-2.14.20-02-bundle.tar.gz' do
    source  'nexus-2.14.20-02-bundle.tar.gz'
    action  :create
    mode    '777'
end

apt_update 'update'

apt_package ['openjdk-8-jre', 'sudo', 'curl'] do
    action :install
end

bash 'extract nexus distribution tar' do
    code <<-EOF
       gzip -d /nexus-2.14.20-02-bundle.tar.gz
       tar -xvf /nexus-2.14.20-02-bundle.tar
       sed -i -e 's/NEXUS_HOME=".."/NEXUS_HOME="\/nexus-2.14.20-02"/' -e 's/#RUN_AS_USER=/RUN_AS_USER=root/' /nexus-2.14.20-02/bin/nexus
       chmod +x /nexus-2.14.20-02/bin/*
    EOF
end

execute 'start nexus' do
    command 'export RUN_AS_USER=root && /nexus-2.14.20-02/bin/nexus start'
end