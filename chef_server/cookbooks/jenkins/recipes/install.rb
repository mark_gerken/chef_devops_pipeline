apt_update 'update'

apt_package ['default-jdk', 'wget', 'sudo', 'apt-transport-https', 'ca-certificates', 'curl', 'software-properties-common', 'git', 'gnupg'] do
    action :install
end

bash 'add jenkins and docker-ce repos to apt' do
    code <<-EOF
        wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
        echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list

        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
        sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"

        sudo apt update --allow-unauthenticated
    EOF
end

apt_package ['jenkins', 'docker-ce'] do
    action :install
    options '--allow-unauthenticated'
end

bash 'configure jenkins to run as root user' do
    code <<-EOF
        sed -i -e 's/JENKINS_USER=\$NAME/JENKINS_USER=root/' /etc/default/jenkins
        sudo chown -R root:root /var/lib/jenkins
        sudo chown -R root:root /var/cache/jenkins
        sudo chown -R root:root /var/log/jenkins
    EOF
end

cookbook_file '/jenkins_data.tar.gz' do
    source 'jenkins_data.tar.gz'
    owner 'root'
    group 'root'
    mode '0755'
    action :create
end

bash 'configure jenkins data' do
    code <<-EOF
        cd /var/lib
        mv /jenkins_data.tar.gz /var/lib/jenkins_data.tar.gz
        gzip -d /var/lib/jenkins_data.tar.gz
        rm -rf /var/lib/jenkins
        tar -xvf /var/lib/jenkins_data.tar
        rm /var/lib/jenkins_data.tar
        chown root:root -R /var/lib/jenkins
    EOF
end

execute 'start jenkins' do
    command 'sudo /etc/init.d/jenkins start'
end