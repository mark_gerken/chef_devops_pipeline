apt_update 'update'

apt_package ['openjdk-8-jre', 'wget'] do
    action :install
end

bash 'download enigma from nexus' do
    code <<-EOF
        wget 'http://nexus:8081/nexus/service/local/artifact/maven/redirect?r=snapshots&g=com.enigma&a=enigma&v=1.0.1-SNAPSHOT&e=jar' -O enigma.jar
    EOF
end

execute 'start enigma' do
    command 'nohup java -jar /enigma.jar > /enigma.out 2>&1 &'
end
