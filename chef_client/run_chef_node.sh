#!/bin/sh

# start the ssh service to allow chef_client nodes to ssh to each other
mkdir /run/sshd
/usr/sbin/sshd -D &

# insert this cotnainer's hostname into the client.rb file
sed -i -e "s/node_name \"HOSTNAME\"/node_name \"$(hostname)\"/" /client.rb
cp /client.rb /knife.rb

# wait for chef_server's chef-infra-server API to be up and running
while sleep 30
do
    echo -n 'checking if chef_server is running... '
    nc -v chefserver 443
    if [ "X$?" = "X0" ]; then
        echo 'chef_server has started!'
        break
    fi
    echo 'chef_server not started yet; sleeping...'
done

# fetch the chef_server's ssl certs
knife ssl fetch

# register this node as a node within the chef_server
while sleep 10
do
    echo -n 'registering node with chef_server... '
    chef-client -c /client.rb > /dev/null 2>&1
    if [ "X$?" = "X0" ]; then
        echo 'success!'
        break
    else
        echo 'error.'
    fi
done

# wait until the chef_server has cookbooks uploaded to it
while sleep 10
do
    echo 'checking to see if chef_server has the proper cookbooks uploaded...'
    number_of_cookbooks=$(knife cookbook list | wc | awk '{print $1}')
    if [ "X$number_of_cookbooks" != "X0" ]; then
        echo "$number_of_cookbooks cookbooks found on chef_server!"
        break
    else
        echo '0 cookbooks present on chef_sever...'
    fi
done

if [ "X$run_list" != "X" ]; then
    # run the specified run_list for this container (defined in the docker-compose.yml file for the service leveaging this image)
    chef-client -o $run_list -c /client.rb
    if [ "$?" != "0" ]; then
        # something happened during the chef-client run; so exit the container
        exit 1
    fi
fi

# indicate that this container has successfully started
touch /chef_client_started.flag

# tail the specified log_file
if [ "X$log_file" != "X" ]; then
    echo "attempting to tail logs..."
    while sleep 10
    do
        tail -f $log_file 2>/dev/null
    done
else
    echo "no logs to tail; tailing /dev/null..."
    tail -f /dev/null
fi