**OUTLINE:**

This repo consists of two docker images and a docker-compose file. The images correspond to a chef-server and a chef-client. The chef-server contains cookbooks to install//configure services and the chef-client image will register the container with the chef-server and conditionally execute chef-client with a specified run_list.

The docker compose file specifies 4 services.

<center>
  <img src="./images/chef_client_overview.PNG">
</center>

* chef-server
  * built off the chef-server image
* jenkins
  * built off the chef-client image. Specifies a run_list to install and start jenkins
  * contains two pre-defined builds. One to build the enigma application and deploy it to nexus, another to deploy the enigma application to the enigma container. 
* nexus
  * built off the chef-client image. Specified a run_list to install and start a nexus repository
  * is empty by default
* enigma
  * built off the chef-client image. Does not specify a run_list to execute on startup.
  * is used by jenkins to deploy the enigma web app

**GETTING STARTED:**

Simply run `docker-compose up` from within the root of this repo. Then run the 'enigma' job in jenkins and once it finishes you can play the enigma puzzle game.

Log in information:

* Jenkins:
  * URL: http://localhost:8080/
  * admin/admin
* Nexus: 
  * URL: http://localhost:8081/nexus
  * admin/admin123
* Enigma:
  * URL: http://localhost:8082/

**Notes:**
* Docker-Container quirks to pipeline:
  * chef-infra-server inside containers isn't officailly supported at the time of this repo's creation
    *  as a consequence we're stuck to an old version of chef-server running in an old version of ubuntu
  * jenkins docker pipelines leverages the docker socket from the docker host rather than installing a full actual docker service within the jenkins container
    * jenkins is also ran as root user instead of default 'jenkins' user since the docker service only allows users with 'docker' group to run docker commands without using sudo if those users existed with those groups when the docker service was started. Since the docker service is the service running on the docker host it's up and running before the jenkins user gets created within the jenkins container. 
